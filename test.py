import sys
from antlr4 import *
from antlr4.InputStream import InputStream
from BasicCircuitLexer import BasicCircuitLexer
from BasicCircuitParser import BasicCircuitParser
from BasicCircuitVisitor import *
from antlr4.tree.Trees import Trees
import ast
# filename = "/home/aksshita/basiccircuit-to-r1cs-lite-master/simple_circuit.circuit"

        # return left * right
		
stream = FileStream(sys.argv[1])
# stream2 = open(str(InputStream(sys.argv[2])),"r")
# stream2 = FileStream(sys.argv[2])
with open(sys.argv[2], 'r') as f:
    contents = f.read().splitlines() 
# print(contents)
# stream2.readlines()
# print(stream2.readline())

def main(argv):
	# print(stream2)

	lexer = BasicCircuitLexer(stream)
	token_stream = CommonTokenStream(lexer)
	# token_stream.fill()
	parser = BasicCircuitParser(token_stream)
# Print tokens as text (EOF is stripped from the end)
	tree = parser.circuit()

	# print(Trees.toStringTree(tree, None, parser))
	# 
	# print([token.text for token in token_stream.tokens][:-1])
	visitor = BasicCircuitVisitor()
	visitor.visit(tree)
	BasicCircuitVisitor.print()
	 
	# a = ast.getIdentifiersList()


if __name__ == '__main__':
	main(sys.argv)




