# Generated from BasicCircuit.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\24")
        buf.write("G\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\3\2\6\2\22\n\2\r\2\16\2\23\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\5\3\37\n\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5")
        buf.write("\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\5\b\67\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\7\bB\n\b\f\b\16\bE\13\b\3\b\2\3\16\t\2\4\6\b\n\f\16\2")
        buf.write("\5\3\2\21\23\4\2\6\6\b\b\4\2\7\7\t\t\2H\2\21\3\2\2\2\4")
        buf.write("\36\3\2\2\2\6 \3\2\2\2\b$\3\2\2\2\n\'\3\2\2\2\f)\3\2\2")
        buf.write("\2\16\66\3\2\2\2\20\22\5\4\3\2\21\20\3\2\2\2\22\23\3\2")
        buf.write("\2\2\23\21\3\2\2\2\23\24\3\2\2\2\24\3\3\2\2\2\25\26\5")
        buf.write("\6\4\2\26\27\7\3\2\2\27\37\3\2\2\2\30\31\5\b\5\2\31\32")
        buf.write("\7\3\2\2\32\37\3\2\2\2\33\34\5\f\7\2\34\35\7\3\2\2\35")
        buf.write("\37\3\2\2\2\36\25\3\2\2\2\36\30\3\2\2\2\36\33\3\2\2\2")
        buf.write("\37\5\3\2\2\2 !\7\20\2\2!\"\7\13\2\2\"#\5\16\b\2#\7\3")
        buf.write("\2\2\2$%\5\n\6\2%&\7\20\2\2&\t\3\2\2\2\'(\t\2\2\2(\13")
        buf.write("\3\2\2\2)*\7\20\2\2*+\7\n\2\2+,\5\16\b\2,\r\3\2\2\2-.")
        buf.write("\b\b\1\2./\7\16\2\2/\67\5\16\b\7\60\67\7\r\2\2\61\67\7")
        buf.write("\20\2\2\62\63\7\4\2\2\63\64\5\16\b\2\64\65\7\5\2\2\65")
        buf.write("\67\3\2\2\2\66-\3\2\2\2\66\60\3\2\2\2\66\61\3\2\2\2\66")
        buf.write("\62\3\2\2\2\67C\3\2\2\289\f\t\2\29:\t\3\2\2:B\5\16\b\n")
        buf.write(";<\f\b\2\2<=\t\4\2\2=B\5\16\b\t>?\f\6\2\2?@\7\17\2\2@")
        buf.write("B\5\16\b\7A8\3\2\2\2A;\3\2\2\2A>\3\2\2\2BE\3\2\2\2CA\3")
        buf.write("\2\2\2CD\3\2\2\2D\17\3\2\2\2EC\3\2\2\2\7\23\36\66AC")
        return buf.getvalue()


class BasicCircuitParser ( Parser ):

    grammarFileName = "BasicCircuit.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "'('", "')'", "'+'", "'*'", "'-'", 
                     "'/'", "':='", "'=='", "'var '", "<INVALID>", "'not '", 
                     "' and '", "<INVALID>", "'input '", "'middle '", "'output '" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "PLUS", "MULT", "MINUS", "DIV", "ASSIGN", "EQQ", "VAR", 
                      "INT", "NOT", "AND", "STRING", "INPUT", "MIDDLE", 
                      "OUTPUT", "WS" ]

    RULE_circuit = 0
    RULE_body = 1
    RULE_constraint = 2
    RULE_newvar = 3
    RULE_var = 4
    RULE_assign = 5
    RULE_expr = 6

    ruleNames =  [ "circuit", "body", "constraint", "newvar", "var", "assign", 
                   "expr" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    PLUS=4
    MULT=5
    MINUS=6
    DIV=7
    ASSIGN=8
    EQQ=9
    VAR=10
    INT=11
    NOT=12
    AND=13
    STRING=14
    INPUT=15
    MIDDLE=16
    OUTPUT=17
    WS=18

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class CircuitContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def body(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BasicCircuitParser.BodyContext)
            else:
                return self.getTypedRuleContext(BasicCircuitParser.BodyContext,i)


        def getRuleIndex(self):
            return BasicCircuitParser.RULE_circuit

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCircuit" ):
                listener.enterCircuit(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCircuit" ):
                listener.exitCircuit(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCircuit" ):
                return visitor.visitCircuit(self)
            else:
                return visitor.visitChildren(self)




    def circuit(self):

        localctx = BasicCircuitParser.CircuitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_circuit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 15 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 14
                self.body()
                self.state = 17 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BasicCircuitParser.STRING) | (1 << BasicCircuitParser.INPUT) | (1 << BasicCircuitParser.MIDDLE) | (1 << BasicCircuitParser.OUTPUT))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def constraint(self):
            return self.getTypedRuleContext(BasicCircuitParser.ConstraintContext,0)


        def newvar(self):
            return self.getTypedRuleContext(BasicCircuitParser.NewvarContext,0)


        def assign(self):
            return self.getTypedRuleContext(BasicCircuitParser.AssignContext,0)


        def getRuleIndex(self):
            return BasicCircuitParser.RULE_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBody" ):
                listener.enterBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBody" ):
                listener.exitBody(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBody" ):
                return visitor.visitBody(self)
            else:
                return visitor.visitChildren(self)




    def body(self):

        localctx = BasicCircuitParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_body)
        try:
            self.state = 28
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 19
                self.constraint()
                self.state = 20
                self.match(BasicCircuitParser.T__0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 22
                self.newvar()
                self.state = 23
                self.match(BasicCircuitParser.T__0)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 25
                self.assign()
                self.state = 26
                self.match(BasicCircuitParser.T__0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ConstraintContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self):
            return self.getToken(BasicCircuitParser.STRING, 0)

        def EQQ(self):
            return self.getToken(BasicCircuitParser.EQQ, 0)

        def expr(self):
            return self.getTypedRuleContext(BasicCircuitParser.ExprContext,0)


        def getRuleIndex(self):
            return BasicCircuitParser.RULE_constraint

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstraint" ):
                listener.enterConstraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstraint" ):
                listener.exitConstraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConstraint" ):
                return visitor.visitConstraint(self)
            else:
                return visitor.visitChildren(self)




    def constraint(self):

        localctx = BasicCircuitParser.ConstraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_constraint)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 30
            self.match(BasicCircuitParser.STRING)
            self.state = 31
            self.match(BasicCircuitParser.EQQ)
            self.state = 32
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class NewvarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var(self):
            return self.getTypedRuleContext(BasicCircuitParser.VarContext,0)


        def STRING(self):
            return self.getToken(BasicCircuitParser.STRING, 0)

        def getRuleIndex(self):
            return BasicCircuitParser.RULE_newvar

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNewvar" ):
                listener.enterNewvar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNewvar" ):
                listener.exitNewvar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNewvar" ):
                return visitor.visitNewvar(self)
            else:
                return visitor.visitChildren(self)




    def newvar(self):

        localctx = BasicCircuitParser.NewvarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_newvar)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 34
            self.var()
            self.state = 35
            self.match(BasicCircuitParser.STRING)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INPUT(self):
            return self.getToken(BasicCircuitParser.INPUT, 0)

        def OUTPUT(self):
            return self.getToken(BasicCircuitParser.OUTPUT, 0)

        def MIDDLE(self):
            return self.getToken(BasicCircuitParser.MIDDLE, 0)

        def getRuleIndex(self):
            return BasicCircuitParser.RULE_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar" ):
                listener.enterVar(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar" ):
                listener.exitVar(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar" ):
                return visitor.visitVar(self)
            else:
                return visitor.visitChildren(self)




    def var(self):

        localctx = BasicCircuitParser.VarContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_var)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << BasicCircuitParser.INPUT) | (1 << BasicCircuitParser.MIDDLE) | (1 << BasicCircuitParser.OUTPUT))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AssignContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def STRING(self):
            return self.getToken(BasicCircuitParser.STRING, 0)

        def ASSIGN(self):
            return self.getToken(BasicCircuitParser.ASSIGN, 0)

        def expr(self):
            return self.getTypedRuleContext(BasicCircuitParser.ExprContext,0)


        def getRuleIndex(self):
            return BasicCircuitParser.RULE_assign

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign" ):
                listener.enterAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign" ):
                listener.exitAssign(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign" ):
                return visitor.visitAssign(self)
            else:
                return visitor.visitChildren(self)




    def assign(self):

        localctx = BasicCircuitParser.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 39
            self.match(BasicCircuitParser.STRING)
            self.state = 40
            self.match(BasicCircuitParser.ASSIGN)
            self.state = 41
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return BasicCircuitParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class IntegerContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(BasicCircuitParser.INT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInteger" ):
                listener.enterInteger(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInteger" ):
                listener.exitInteger(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInteger" ):
                return visitor.visitInteger(self)
            else:
                return visitor.visitChildren(self)


    class AndExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def AND(self):
            return self.getToken(BasicCircuitParser.AND, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BasicCircuitParser.ExprContext)
            else:
                return self.getTypedRuleContext(BasicCircuitParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAndExpr" ):
                listener.enterAndExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAndExpr" ):
                listener.exitAndExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAndExpr" ):
                return visitor.visitAndExpr(self)
            else:
                return visitor.visitChildren(self)


    class VariableContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(BasicCircuitParser.STRING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariable" ):
                listener.enterVariable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariable" ):
                listener.exitVariable(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVariable" ):
                return visitor.visitVariable(self)
            else:
                return visitor.visitChildren(self)


    class SubExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(BasicCircuitParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubExpr" ):
                listener.enterSubExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubExpr" ):
                listener.exitSubExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSubExpr" ):
                return visitor.visitSubExpr(self)
            else:
                return visitor.visitChildren(self)


    class MultExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BasicCircuitParser.ExprContext)
            else:
                return self.getTypedRuleContext(BasicCircuitParser.ExprContext,i)

        def MULT(self):
            return self.getToken(BasicCircuitParser.MULT, 0)
        def DIV(self):
            return self.getToken(BasicCircuitParser.DIV, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultExpr" ):
                listener.enterMultExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultExpr" ):
                listener.exitMultExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMultExpr" ):
                return visitor.visitMultExpr(self)
            else:
                return visitor.visitChildren(self)


    class NotExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(BasicCircuitParser.NOT, 0)
        def expr(self):
            return self.getTypedRuleContext(BasicCircuitParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNotExpr" ):
                listener.enterNotExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNotExpr" ):
                listener.exitNotExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotExpr" ):
                return visitor.visitNotExpr(self)
            else:
                return visitor.visitChildren(self)


    class SumExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a BasicCircuitParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(BasicCircuitParser.ExprContext)
            else:
                return self.getTypedRuleContext(BasicCircuitParser.ExprContext,i)

        def PLUS(self):
            return self.getToken(BasicCircuitParser.PLUS, 0)
        def MINUS(self):
            return self.getToken(BasicCircuitParser.MINUS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSumExpr" ):
                listener.enterSumExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSumExpr" ):
                listener.exitSumExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSumExpr" ):
                return visitor.visitSumExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = BasicCircuitParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 12
        self.enterRecursionRule(localctx, 12, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [BasicCircuitParser.NOT]:
                localctx = BasicCircuitParser.NotExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 44
                self.match(BasicCircuitParser.NOT)
                self.state = 45
                self.expr(5)
                pass
            elif token in [BasicCircuitParser.INT]:
                localctx = BasicCircuitParser.IntegerContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 46
                self.match(BasicCircuitParser.INT)
                pass
            elif token in [BasicCircuitParser.STRING]:
                localctx = BasicCircuitParser.VariableContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 47
                self.match(BasicCircuitParser.STRING)
                pass
            elif token in [BasicCircuitParser.T__1]:
                localctx = BasicCircuitParser.SubExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 48
                self.match(BasicCircuitParser.T__1)
                self.state = 49
                self.expr(0)
                self.state = 50
                self.match(BasicCircuitParser.T__2)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 65
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 63
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
                    if la_ == 1:
                        localctx = BasicCircuitParser.SumExprContext(self, BasicCircuitParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 54
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 55
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==BasicCircuitParser.PLUS or _la==BasicCircuitParser.MINUS):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 56
                        localctx.right = self.expr(8)
                        pass

                    elif la_ == 2:
                        localctx = BasicCircuitParser.MultExprContext(self, BasicCircuitParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 57
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 58
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==BasicCircuitParser.MULT or _la==BasicCircuitParser.DIV):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 59
                        localctx.right = self.expr(7)
                        pass

                    elif la_ == 3:
                        localctx = BasicCircuitParser.AndExprContext(self, BasicCircuitParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 60
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 61
                        self.match(BasicCircuitParser.AND)
                        self.state = 62
                        localctx.right = self.expr(5)
                        pass

             
                self.state = 67
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[6] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 4)
         




