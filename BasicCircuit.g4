grammar BasicCircuit;

// parser


circuit   : body+ ;

body :  constraint ';'  
		|  newvar ';' 
		|  assign ';' 
		;

constraint :  STRING EQQ expr  ;


newvar 	: var  STRING ;													 
												


var : INPUT | OUTPUT | MIDDLE ;  // divides var into types, hence variable is defined as input w1 instead of var input w1 e.t.c 

assign 	: STRING ASSIGN expr	;															

expr	:   left=expr op=( PLUS | MINUS) right=expr   				#SumExpr
		|	left=expr op=(MULT | DIV ) right=expr							#MultExpr
		|   NOT expr                                #NotExpr
		|   left=expr AND right=expr																#AndExpr
		|   INT                                     #Integer
		|   STRING									#Variable
		|   '(' expr ')'                            #SubExpr
		;

// lexer

PLUS	:	'+';
MULT	:	'*';
MINUS	:	'-';
DIV		:	'/';
ASSIGN	:	':=';
EQQ		:	'==';
VAR     :   'var ' ;
INT     :   [0-9]+ ;         // match integers
NOT     :   'not ' ;
AND     :   ' and ' ;
STRING  :  	[0-9a-zA-Z]+ ;
INPUT   :   'input ' ;
MIDDLE  :	'middle ' ;
OUTPUT  :   'output ';


WS  :   [ \t\n\r]+ -> skip ; // toss out all whitespace

	
