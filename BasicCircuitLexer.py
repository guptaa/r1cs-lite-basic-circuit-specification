# Generated from BasicCircuit.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\24")
        buf.write("s\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3")
        buf.write("\b\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3")
        buf.write("\f\6\fB\n\f\r\f\16\fC\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3")
        buf.write("\16\3\16\3\16\3\16\3\17\6\17R\n\17\r\17\16\17S\3\20\3")
        buf.write("\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22")
        buf.write("\3\23\6\23n\n\23\r\23\16\23o\3\23\3\23\2\2\24\3\3\5\4")
        buf.write("\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17")
        buf.write("\35\20\37\21!\22#\23%\24\3\2\5\3\2\62;\5\2\62;C\\c|\5")
        buf.write("\2\13\f\17\17\"\"\2u\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2")
        buf.write("\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2")
        buf.write("\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31")
        buf.write("\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2")
        buf.write("\2\2\2#\3\2\2\2\2%\3\2\2\2\3\'\3\2\2\2\5)\3\2\2\2\7+\3")
        buf.write("\2\2\2\t-\3\2\2\2\13/\3\2\2\2\r\61\3\2\2\2\17\63\3\2\2")
        buf.write("\2\21\65\3\2\2\2\238\3\2\2\2\25;\3\2\2\2\27A\3\2\2\2\31")
        buf.write("E\3\2\2\2\33J\3\2\2\2\35Q\3\2\2\2\37U\3\2\2\2!\\\3\2\2")
        buf.write("\2#d\3\2\2\2%m\3\2\2\2\'(\7=\2\2(\4\3\2\2\2)*\7*\2\2*")
        buf.write("\6\3\2\2\2+,\7+\2\2,\b\3\2\2\2-.\7-\2\2.\n\3\2\2\2/\60")
        buf.write("\7,\2\2\60\f\3\2\2\2\61\62\7/\2\2\62\16\3\2\2\2\63\64")
        buf.write("\7\61\2\2\64\20\3\2\2\2\65\66\7<\2\2\66\67\7?\2\2\67\22")
        buf.write("\3\2\2\289\7?\2\29:\7?\2\2:\24\3\2\2\2;<\7x\2\2<=\7c\2")
        buf.write("\2=>\7t\2\2>?\7\"\2\2?\26\3\2\2\2@B\t\2\2\2A@\3\2\2\2")
        buf.write("BC\3\2\2\2CA\3\2\2\2CD\3\2\2\2D\30\3\2\2\2EF\7p\2\2FG")
        buf.write("\7q\2\2GH\7v\2\2HI\7\"\2\2I\32\3\2\2\2JK\7\"\2\2KL\7c")
        buf.write("\2\2LM\7p\2\2MN\7f\2\2NO\7\"\2\2O\34\3\2\2\2PR\t\3\2\2")
        buf.write("QP\3\2\2\2RS\3\2\2\2SQ\3\2\2\2ST\3\2\2\2T\36\3\2\2\2U")
        buf.write("V\7k\2\2VW\7p\2\2WX\7r\2\2XY\7w\2\2YZ\7v\2\2Z[\7\"\2\2")
        buf.write("[ \3\2\2\2\\]\7o\2\2]^\7k\2\2^_\7f\2\2_`\7f\2\2`a\7n\2")
        buf.write("\2ab\7g\2\2bc\7\"\2\2c\"\3\2\2\2de\7q\2\2ef\7w\2\2fg\7")
        buf.write("v\2\2gh\7r\2\2hi\7w\2\2ij\7v\2\2jk\7\"\2\2k$\3\2\2\2l")
        buf.write("n\t\4\2\2ml\3\2\2\2no\3\2\2\2om\3\2\2\2op\3\2\2\2pq\3")
        buf.write("\2\2\2qr\b\23\2\2r&\3\2\2\2\6\2CSo\3\b\2\2")
        return buf.getvalue()


class BasicCircuitLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    PLUS = 4
    MULT = 5
    MINUS = 6
    DIV = 7
    ASSIGN = 8
    EQQ = 9
    VAR = 10
    INT = 11
    NOT = 12
    AND = 13
    STRING = 14
    INPUT = 15
    MIDDLE = 16
    OUTPUT = 17
    WS = 18

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "'('", "')'", "'+'", "'*'", "'-'", "'/'", "':='", "'=='", 
            "'var '", "'not '", "' and '", "'input '", "'middle '", "'output '" ]

    symbolicNames = [ "<INVALID>",
            "PLUS", "MULT", "MINUS", "DIV", "ASSIGN", "EQQ", "VAR", "INT", 
            "NOT", "AND", "STRING", "INPUT", "MIDDLE", "OUTPUT", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "PLUS", "MULT", "MINUS", "DIV", 
                  "ASSIGN", "EQQ", "VAR", "INT", "NOT", "AND", "STRING", 
                  "INPUT", "MIDDLE", "OUTPUT", "WS" ]

    grammarFileName = "BasicCircuit.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


