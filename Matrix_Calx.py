import numpy
import sys
# import BasicCircuitVisitor
def ComputeMatrices(LeftInputs,RightInputs,Lin,l,NumberGates,N,numberLeftInputs,numberRightInputs):
 #LeftInputs and RightInputs are matrices that,in line i,contain list of left and right coefficients of multiplication gate i, Lin,l,NumberGates,N are paramtere of the circuit 
 #If each multiplication gate can have only one left input and only one right input
 #compute L
 L=numpy.zeros(shape=(N,N))
 i=0
 j=0
 #First step, -ILin+1
 while i<(Lin):
    j=0
    while j<(Lin):
       if(i==j):
         L[i][j]=-1
       j=j+1
    i=i+1
 #Now compute rows from Lin+2 to l
 j=Lin
 count=0
 while j<l:
   i=0
   while i<l:
       #k=0
       #while k<(len(LeftInputs[count])):
         #if (i==(k+1)) :
           #L[j][i]=-LeftInputs[count][k]
         #k=k+1
        if (i<(numberLeftInputs+1)):
          # print(j,count)
          L[j][count]=-LeftInputs[count][0]     
        i=i+1
   count=count+1
   j=j+1 
 #Now compute rows from l+1 to n
 j=l
 i=0
 count=0
 while j<N:
   i=0
   while i<N:
       #k=0
       #while k<(len(LeftInputs[count])):
         #if (i==(k+1)) :
           #L[j][i]=-LeftInputs[count][k]
         #k=k+1
        if (i<(numberLeftInputs+1)):
          print(j,count)
          L[j][count]=-LeftInputs[count][0]     
        i=i+1
   count=count+1
   j=j+1 
 #print L
 print("Print L")
 print(L)
 #compute E
 l=N-NumberGates
 E=numpy.zeros(shape=(l,N))
 i=0
 j=0
 while i<l:
    E[i][0]=1
    i=i+1
 #Compute R
 # Set rows from 1 to l equal to -matrix E
 R=numpy.zeros(shape=(N,N))
 i=0
 j=0
 while i<l:
    j=0
    while j<N:
       if (E[i][j]==1):
         R[i][j]=-E[i][j]
       j=j+1
    i=i+1
 #Set rows from l+1 to n
 j=l
 i=0
 count=0
 while j<N:
   i=0
   while i<N:
     #elif (i==1):
      #R[j][i]=-RightInputs[count][0]
       #k=0
       #while k<(len(RightInputs[count])):
         #if (i==(k+1)) :
           #R[j][i]=-RightInputs[count][k]
         #k=k+1
       if (i<(numberRightInputs+1)):
         R[j][count+numberLeftInputs]=-RightInputs[count][0]     
       i=i+1
   count=count+1
   j=j+1 
 print("Print R")
 print(R)
 
 sys.stdout = open("matrices.txt", "w")
 print("L=")
 print(L)
 print("\n\n\n\n")
 print("R=")
 print(R)
 sys.stdout.close()



#ComputeMatrices([[5],[3],[4],[9]],[[7],[5],[3],[4]],6,8,4,12,4,4) #2 mult gates, n=7 Lin=4 l=5
