# Generated from BasicCircuit.g4 by ANTLR 4.7.2
from antlr4 import *
import pandas as pd
import numpy as np
import Matrix_Calx
import test
# from CircuitVisitor import *
if __name__ is not None and "." in __name__:
    from .BasicCircuitParser import BasicCircuitParser
else:
    from BasicCircuitParser import BasicCircuitParser

# This class defines a complete generic visitor for a parse tree produced by BasicCircuitParser.
input_assignments={} #Variable to fetch and store values of input variables in instance and middle variables 
for t in test.contents: 
    if ":" in t:
        temp = t.partition(" : ")
        key = temp[0]
        val = temp[2]
        input_assignments[key]=val
expressions=pd.DataFrame(columns=['output_val','expr']) #Variable to store and compare initial expressions to be calculated
operators = ['+','*','/','and','-','not']
inputvar=[]
outputvar=[]
output_assignment=pd.DataFrame(columns=['output_val','expr1', 'expr2' , 'value'])
middlevar=[]
middle_assignment=pd.DataFrame(columns=['middle','expr', 'value'])
# output_assignment={}
coefficient={}
constraint=[]
leftvar = []
temp_int = []
rightvar =[]
body_sep=[]
num_gates=0
N = 0
l = 0
n=0
lout=0
lin=0
lmid=0
i=0 #counter


class BasicCircuitVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by BasicCircuitParser#circuit.
    def visitCircuit(self, ctx:BasicCircuitParser.CircuitContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#body.
    def visitBody(self, ctx:BasicCircuitParser.BodyContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#constraint.
    def visitConstraint(self, ctx:BasicCircuitParser.ConstraintContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#newvar.
    def visitNewvar(self, ctx:BasicCircuitParser.NewvarContext):
        global lin
        global lmid
        global lout
        temp_op=[]
        if "output" in ctx.getText():
            temp_op = ctx.getText().partition(" ")
            # outputvar.loc[len(outputvar.index)] = [j, sub_expr, value]
            outputvar.append(temp_op[2])
            lout+=1
        elif "input" in ctx.getText():
            temp_op = ctx.getText().partition(" ")
            inputvar.append(temp_op[2])
            lin+=1
        elif "middle" in ctx.getText():   
            temp_op = ctx.getText().partition("middle ")
            middlevar.append(temp_op[2])
            lmid+=1
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#var.
    def visitVar(self, ctx:BasicCircuitParser.VarContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#assign.
    def visitAssign(self, ctx:BasicCircuitParser.AssignContext):
        temp = ctx.getText().partition(":=")
        expressions.loc[len(expressions.index)] = [temp[0],temp[2]]
        # print(expressions)
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#Integer.
    def visitInteger(self, ctx:BasicCircuitParser.IntegerContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#AndExpr.
    def visitAndExpr(self, ctx:BasicCircuitParser.AndExprContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#Variable.
    def visitVariable(self, ctx:BasicCircuitParser.VariableContext):
        # print(ctx.getText())
        return self.visitChildren(ctx)


        
    # Visit a parse tree produced by BasicCircuitParser#SubExpr.
    def visitSubExpr(self, ctx:BasicCircuitParser.SubExprContext):
        # sub_expr=ctx.getText().replace('(', '').replace(')', '')
        # # print(sub_expr)
        # global i 
        # j = "j"
        # for op in operators:
        #     if op in sub_expr:
        #         sub_expr_tmp=sub_expr.partition(op)
        #         # print(sub_expr_tmp)
        #         if op == "*":
        #             i=i+1
        #             value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
        #             j=j+str(i)
        #             middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
        #             # print(middle_assignment)
        #             break
        #         elif op == "+":
        #             # test.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[1])
        #             i=i+1
        #             value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
        #             j=j+str(i)
        #             middle_assignment[j]=value
        #             print(middle_assignment)
        #             break
                    
        #         elif op == "-":
        #             # test.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[1])
        #             i=i+1
        #             value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
        #             j=j+str(i)
        #             middle_assignment[j]=value
        #             print(middle_assignment)
        #             break

        #         elif op == "/":
        #             i=i+1
        #             value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
        #             j=j+str(i)
        #             middle_assignment[j]=value
        #             print(middle_assignment)
        #             break

        #         elif op == "and":
        #             i=i+1
        #             value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
        #             j=j+str(i)
        #             middle_assignment[j]=value
        #             print(middle_assignment)
        #             break

        #         elif op == "not":
        #             i=i+1
        #             value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
        #             j=j+str(i)
        #             middle_assignment[j]=value
        #             print(middle_assignment)
        #             break
        #         else:
        #             print("invalid exp in sub expression")
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#MultExpr.
    def visitMultExpr(self, ctx:BasicCircuitParser.MultExprContext):
        # print(ctx.getText())
        global num_gates
        flag =0
        left = ctx.left.getText()#.replace('(', '').replace(')', '')
        right = ctx.right.getText()#.replace('(', '').replace(')', '')
        op = ctx.op.text
        val_right=0
        val_left=0
        if "(" in right and (")" in right) and (not ("(" in left and (")" in left))):
            val_right = BasicCircuitVisitor.visitSub_Expr(right)
            if (middle_assignment['expr'].str.contains(right,regex=False).any()==False) and (not left.isdigit()) and left in middlevar:
                val_left = int(BasicCircuitVisitor.getVal(left))
                leftvar.append(left)
                rightvar.append(middle_assignment['middle'][0])
                coefficient[left]=1
                flag=1
            elif (middle_assignment['expr'].str.contains(right,regex=False).any()==False) and (left.isdigit()):
                val_left = int(left)
                leftvar.append(left)
                rightvar.append(middle_assignment['middle'][0])
                flag=1
        elif "(" in left and (")" in left) and (not ("(" in right and (")" in right))):
            val_left = BasicCircuitVisitor.visitSub_Expr(left)
            # print(val_left)
            if (middle_assignment['expr'].str.contains(left,regex=False).any()==False) and (not right.isdigit()):
                val_right = int(input_assignments.get(right))
                leftvar.append(middle_assignment['middle'][0])
                rightvar.append(right)
                coefficient[right]=1
                flag=1
        elif "(" in right and (")" in right) and ("(" in left) and (")" in left):
            val_right = BasicCircuitVisitor.visitSub_Expr(right)
            val_left = BasicCircuitVisitor.visitSub_Expr(left)
            if (middle_assignment['expr'].str.contains(left,regex=False).any()==False) and (middle_assignment['expr'].str.contains(right,regex=False).any()==False):
                leftftvar.append(middle_assignment['middle'][0])
                rightvar.append(middle_assignment['middle'][1])
                flag=1
        elif (not ("(" in right and (")" in right))) and (not ("(" in left) and (")" in left)):
            val_right=int(input_assignments.get(right))
            val_left=int(input_assignments.get(left))
            leftvar.append(left)
            rightvar.append(right)
            flag=1
        elif (not right.isdigit()) and (not left.isdigit()):
            leftvar.append(left)
            rightvar.append(right)
            val_right =int(input_assignments.get(left))
            val_left = int(input_assignments.get(right))
            flag=1
        else :
            flag=0
            print("skip")
        if flag==1:
            if expressions['output_val'].str.contains(outputvar,regex=False).any()==False and expressions['expr'].str.contains(ctx.getText(),regex=True).any()==True and (expressions['output_val'].str.contains(middlevar,regex=False).any()==False and expressions['expr'].str.contains(ctx.getText(),regex=False).any()==False): 
                num_gates+=1
                output_assignment.loc[len(output_assignment.index)] = [outputvar, left, right , val_left*val_right]     
            elif expressions['output_val'].str.contains(middlevar,regex=False).any()==False and expressions['expr'].str.contains(ctx.getText(),regex=True).any()==False:
                num_gates+=1
                output_assignment.loc[len(output_assignment.index)] = [middlevar, left, right , val_left*val_right]
        # print(output_assignment)
        return self.visitChildren(ctx)
    
    


    # Visit a parse tree produced by BasicCircuitParser#NotExpr.
    def visitNotExpr(self, ctx:BasicCircuitParser.NotExprContext):
        # left = ctx.left.getText()#.replace('(', '').replace(')', '') right =
        # ctx.right.getText()#.replace('(', '').replace(')', '') op =
        # ctx.op.text if "(" in right and (")" in right) and (not ("(" in left
        # and (")" in left))): val_right =
        # BasicCircuitVisitor.visitNot_Expr(right) if
        # (middle_assignment['expr'].str.contains(right,regex=False).any()==False)
        # and (not left.isdigit()): val_left =
        # int(input_assignments.get(left)) leftvar.append(left)
        # rightvar.append(middle_assignment['middle'][0]) coefficient[left]=1
        # output_assignment.loc[len(output_assignment.index)] = [outputvar,
        # left, right , val_left*val_right] elif "(" in left and (")" in left)
        # and (not ("(" in right and (")" in right))): val_left =
        # BasicCircuitVisitor.visitNot_Expr(left) if
        # (middle_assignment['expr'].str.contains(left,regex=False).any()==False)
        # and (not right.isdigit()): val_right =
        # int(input_assignments.get(right))
        # leftvar.append(middle_assignment['middle'][0])
        # rightvar.append(right) coefficient[right]=1
        # output_assignment.loc[len(output_assignment.index)] = [outputvar,
        # left, right , val_left*val_right]    elif "(" in right and (")" in
        # right) and ("(" in left) and (")" in left): val_right =
        # BasicCircuitVisitor.visitNot_Expr(right) val_left =
        # BasicCircuitVisitor.visitNot_Expr(left) if
        # (middle_assignment['expr'].str.contains(left,regex=False).any()==False)
        # and
        # (middle_assignment['expr'].str.contains(right,regex=False).any()==False):
        # leftvar.append(middle_assignment['middle'][0])
        # rightvar.append(middle_assignment['middle'][1])
        # output_assignment.loc[len(output_assignment.index)] = [outputvar,
        # left, right , val_left*val_right] print(output_assignment)

        # else: 
        #     print("skip")
        # print(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by BasicCircuitParser#SumExpr.
    def visitSumExpr(self, ctx:BasicCircuitParser.SumExprContext):
        # print(ctx.getText())
        left = ctx.left.getText()#.replace('(', '').replace(')', '')
        right = ctx.right.getText()#.replace('(', '').replace(')', '')
        op = ctx.op.text
        if "(" in right and (")" in right) and (not ("(" in left and (")" in left))):
            val_right = BasicCircuitVisitor.visitSub_Expr(right)
            if (middle_assignment['expr'].str.contains(right,regex=False).any()==False) and (not left.isdigit()):
                val_left = int(BasicCircuitVisitor.getVal(left))
                leftvar.append(left)
                rightvar.append(middle_assignment['middle'][0])
                coefficient[left]=1
        elif "(" in left and (")" in left) and (not ("(" in right and (")" in right))):
            val_left = BasicCircuitVisitor.visitSub_Expr(left)
            if (middle_assignment['expr'].str.contains(left,regex=False).any()==False) and (not right.isdigit()):
                val_right = int(input_assignments.get(right))
                leftvar.append(middle_assignment['middle'][0])
                rightvar.append(right)
                coefficient[right]=1
        elif "(" in right and (")" in right) and ("(" in left) and (")" in left):
            val_right = BasicCircuitVisitor.visitSub_Expr(right)
            val_left = BasicCircuitVisitor.visitSub_Expr(left)
            if (middle_assignment['expr'].str.contains(left,regex=False).any()==False) and (middle_assignment['expr'].str.contains(right,regex=False).any()==False):
                leftvar.append(middle_assignment['middle'][0])
                rightvar.append(middle_assignment['middle'][1])
        return self.visitChildren(ctx)

    def visitMult_Expr(a, b):  # calculating coefficients other than 1
        left = a
        right = b
        if a.isdigit() and (not b.isdigit()):
            left = int(a)
            right = int(input_assignments.get(right))
            rightvar.append(b)
            coefficient[b]=left
        elif b.isdigit() and (not a.isdigit()):
            right = int(b)
            left = int(input_assignments.get(left))
            leftvar.append(a)
            coefficient[a]=1
        elif "(" in a and (")" in a) and (not ("(" in b and (")" in b))):
            left = BasicCircuitVisitor.visitSub_Expr(left)
            if b == None:
                right = 1
            elif not b.isdigit():
                right == int(input_assignments.get(right))
            elif b.isdigit():
                right = int(b)
        elif "(" in b and (")" in b) and (not ("(" in a and (")" in a))):    

            right = BasicCircuitVisitor.visitSub_Expr(right)

            if a== None:
                left = 1
            elif not a.isdigit():
                left == int(input_assignments.get(left))
            elif a.isdigit():
                left = int(a)
        else:
            left = BasicCircuitVisitor.visitSub_Expr(left)
            right = BasicCircuitVisitor.visitSub_Expr(right)
        return(left * right)
    def visitAdd_Expr(a, b):  # calculating coefficients other than 1 Check if coefficient of + has to be added or not
        left = a
        right = b
        if a.isdigit() and (not b.isdigit()):
            left = int(a)
            right = int(input_assignments.get(right))
            rightvar.append(b)
            coefficient[b]=left
        elif b.isdigit() and (not a.isdigit()):
            right = int(b)
            left = int(input_assignments.get(left))
            leftvar.append(a)
            coefficient[a]=1
        elif "(" in a and (")" in a) and (not ("(" in b and (")" in b))):
            left = BasicCircuitVisitor.visitSub_Expr(left)
            if b == None:
                right = 1
            elif not b.isdigit():
                right == int(input_assignments.get(right))
            elif b.isdigit():
                right = int(b)
        elif "(" in b and (")" in b) and (not ("(" in a and (")" in a))):    

            right = BasicCircuitVisitor.visitSub_Expr(right)

            if a== None:
                left = 1
            elif not a.isdigit():
                left == int(input_assignments.get(left))
            elif a.isdigit():
                left = int(a)
        else:
            left = BasicCircuitVisitor.visitSub_Expr(left)
            right = BasicCircuitVisitor.visitSub_Expr(right)
        return((left + right)*1)
    def visitAnd_Expr(a, b):  # calculating coefficients other than 1
        left = a
        right = b
        print(right)
        if a.isdigit() and (not b.isdigit()):
            left = int(a)
            right = int(input_assignments.get(right))
            # coefficient[right]=left
        elif b.isdigit() and (not a.isdigit()):
            right = int(b)
            left = int(input_assignments.get(left))
            # coefficient[left]=right
        # print(left)
        # print(right)
        return(left and right)
    def visitNot_Expr(a):  # calculating coefficients other than 1
        var=~A+2
        # print(right)
        return(var)
    
    def visitSub_Expr(body): #Evaluate  sub expressions
        sub_expr=body.replace('(', '').replace(')', '')
        # value=0
        # print(body)
        global i 
        j = "j"
        for op in operators:
            if op in sub_expr:
                sub_expr_tmp=sub_expr.partition(op)
                # print(sub_expr_tmp)
                if op == "*":
                    i=i+1
                    value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    # print(value)
                    j=j+str(i)
                    middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
                    break
                elif op == "+":
                    i=i+1
                    value=BasicCircuitVisitor.visitAdd_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    j=j+str(i)
                    middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
                    break
                    
                # elif op == "-":
                #     # test.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[1])
                #     i=i+1
                #     value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
                #     j=j+str(i)
                #     middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
                #     break

                # elif op == "/":
                #     i=i+1
                #     value=BasicCircuitVisitor.visitMult_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
                #     j=j+str(i)
                #     middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
                #     break

                elif op == "and":
                    i=i+1
                    value=BasicCircuitVisitor.visitAdd_Expr(sub_expr_tmp[0],sub_expr_tmp[2])
                    
                    j=j+str(i)
                    middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
                    break

                elif op == "not":
                    i=i+1
                    value=BasicCircuitVisitor.visitNot_Expr(sub_expr_tmp[1])
                    
                    j=j+str(i)
                    middle_assignment.loc[len(middle_assignment.index)] = [j, sub_expr, value]
                    break
                else:
                    print("invalid exp in sub expression")
        # print(middle_assignment)
        return value

    def getVal(value): #Fetch values of variables
        if value in input_assignments:
            ret_val=input_assignments.get(value)
        elif value in middlevar:
            ret_val=output_assignment['value'][0]
        
        else:
            print("variable not available")
        return ret_val
    
    def extractDigits(lst): #Format left and right inputs in list format for Matrix_Calx format
        return list(map(lambda el:[el], lst))     



    def print(): # Call Matrix_Calx class to compute L and R Matrices
        cl=[]
        cr=[]
        l=lin+lout+1
        N=num_gates+l
        # if coefficient[leftvar] in leftvar:
        for x in leftvar:
            if x in coefficient.keys():
                cl.append(coefficient.get(x))
            elif x not in coefficient.keys():
                coefficient[x]=1
                cl.append(coefficient.get(x))

            else:
                continue
        for x in rightvar:
            if x in coefficient.keys():
                cr.append(coefficient.get(x))
            elif x not in coefficient.keys():
                coefficient[x]=1
                cr.append(coefficient.get(x))
            else:
                continue        
        cl_L=BasicCircuitVisitor.extractDigits(cl) 
        cr_R=BasicCircuitVisitor.extractDigits(cr)

        # print(cl_L)
        # print(cr_R)
        Matrix_Calx.ComputeMatrices(cl_L,cr_R,lin,l,num_gates,N,len(cl_L),len(cr_R))     






del BasicCircuitParser